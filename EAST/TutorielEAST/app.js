(function($){

	// variables des boutons récuperés par ID
	var $btn = $("#btn");
	var $first = $("#first");
	var $prev = $("#prev");
	var $last = $("#last");
	var $next = $("#next");
	var $grand = $("#agrandir");
	var $retrecir = $("#A--");
	
	
	var words = null;
	var isRecording = false;

	if("webkitSpeechRecognition" in window){
		var recognition = new webkitSpeechRecognition();
		// reglage de la langue en Français
		recognition.lang = "fr-FR";
		// Activation de la reception micro en continue
		recognition.continuous = true;
		// desactivation de l'interpretation des résultats
		recognition.interimResults = false;
		
			if(!isRecording){
				// Demarrage de l'API speech
				recognition.start();
				isRecording = true;
			}
		
		recognition.onresult = function(e){
	
		// recuperation de l'interpretation speech sous forme de chaine de caractere
			for(var i = e.resultIndex; i < e.results.length; i++){
				var transcript = e.results[i][0].transcript;
				if(e.results[i].isFinal){
					// découpage des mots dans un tableau
					words = transcript.split(' ');

				}
			}
			// test dans la console pour vérifier les mots récuperer
			console.log(words);

			
			// liste des actions par commandes vocales
			// récuperation des index 0 et 1 pour corrigé les imperfection de l'interpretation speech
			
			
			switch (words[0] || words[1] ) {
				
				case 'suivant':
					$next.click();
				break;
				
				
				case 'précédent':
					$prev.click();
				break;
				
				
				case 'début':
					$first.click();
				break;
				
				
				case 'fin':
					$last.click();
				break;
				

				case 'final':
					$last.click();
				break;
				
				
				case 'finale':
					$last.click();
				break;
				
				
				case 'zoomer':
					$grand.click();
				break;
				
				
				case 'dézoomer':
					$retrecir.click();
				break;
				
				
				case 'agrandir':
					$grand.click();
				break;
				
				
				case 'rétrécir':
					$retrecir.click();
				break;

				
				
			}
		
		};	
	}

})(jQuery);
